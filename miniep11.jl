function palindromo(string)

    if string == ""
        return true
    end


    invertido = []
   
    pontuacao = replace(string, r"[,.;:!?]" => "")
    minusculo = lowercase(pontuacao)
    a = replace(minusculo, r"[áàãâ]" => "a")
    e = replace(a, r"[éèê]" => "e")
    i = replace(e, r"[íìî]" => "i")
    o = replace(i, r"[óòô]" => "o")
    u = replace(o, r"[úùû]" => "u")
    traco = replace(u, "-" => "")
    space = replace(traco, " " => "")
    
    normal = split(space, "")
    
    y = length(normal)
    
    for i in 1:length(normal)
       
       push!(invertido, normal[y])
       y = y - 1
       
    end
 
    if normal == invertido
        return true
    else
        return false
    end
    
    
end

using Test
    
    function testaPalindromo()
    
    @test palindromo("")

    @test palindromo("ovo")

    @test palindromo("MiniEP11")

    @test palindromo("Socorram-me, subi no ônibus em Marrocos!")

    @test palindromo("A mãe te ama.")
 
    @test palindromo("Passei em MAC0110!")
    
    println("Final dos testes")

end

testaPalindromo()
